defmodule Fasta do
  @id_example ">Rosalind_1111"

  def to_list(file) do
    case File.read(file) do
      {:ok, string} ->
        String.replace(string, "\r", "")
        |> String.replace("\n", "")
        |> String.split(">", trim: true)
        |> Enum.map(fn x -> ">" <> x end)
        |> Enum.map(fn x ->
          String.split_at(x, String.length(@id_example))
        end)
      {:error, :enoent} ->
        IO.puts "file doesn't exist"
    end
  end
end