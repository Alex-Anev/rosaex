defmodule Controller do

  def start do
    spawn(fn -> tasker() end)
    |> Process.register(:tasker)
  end

  def send(data) do
    send(:tasker, {:subs, data})
  end

  # tasker sends data to the desired task
  # {:task, data}
  def tasker do
    receive do
      {:dna, data} -> Dna.start(data)
      # {:fib, data} -> ## done
      {:gc, data} -> Gc.start(data)
      {:hamm, {s, t}} -> Hamm.start(s, t)
      # {:iprb, data} -> ##
      {:prot, data} -> Prot.start(data)
      {:prtm, data} -> Prtm.start(data)
      {:revc, data} -> Revc.start(data)
      {:rna, data} -> Rna.start(data)
      {:subs, {s, t}} -> Subs.start(s, t)
      # {:splc, {dna, introns}} -> Splc.start(dna, introns)
      # {:tran, {s1, s2}} -> Tran.start(s1, s2)
      # {:sseq, {dna, substr}} -> Sseq.start(dna, substr)
      # {:mrna, data} -> Mrna.start(data)
      # {:perm, data} -> Perm.start(data)
      # {:pper, {n, k}} -> Pperm.start(n, k)
    end
    tasker()
  end

end

# https://www.biostars.org