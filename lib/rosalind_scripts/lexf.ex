defmodule Lexf do

  @alphabet ["A", "C", "G", "T"]
  # def alphabet, do: ["A", "B", "C", "D", "E", "F", "G", "H"]
  
  # TODO: store this in a process
  @n 2

  def start(alphabet \\ @alphabet) do
    main_loop(alphabet, [])
  end

  def main_loop([], result), do: Enum.reverse result
  def main_loop([hd|tl], result) do

    r = copy_loop(@alphabet, hd, [], @n)

    main_loop(tl, [r | result])
  end

  # \/ crashes BEAM
  def copy_loop([], _, result, acc) when acc == 0, do: Enum.reverse result
  #################
  def copy_loop([], main, result, acc), do: copy_loop(@alphabet, main, result, acc)
  def copy_loop(_, main, result, acc) when acc == 0, do: copy_loop(@alphabet, main, result, @n)
  def copy_loop([hd|tl], main, result, acc) do
    copy_loop(tl, main, [main<>hd|result], acc - 1)
  end
end