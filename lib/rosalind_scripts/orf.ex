defmodule Orf do
  
  defp stop_codons, do: ["UAA", "UAG", "UGA"]
  defp string_to_codons(str), do: String.split(str, ~r/.{3}/, include_captures: true, trim: true)
  
  # expects single string
  def start(data) do
    [data, Revc.start(data)]
    |> Enum.map(fn dna -> 
      dna
      |> Rna.start()
      |> String.split("", trim: true)
      |> find_start_codon([])
    end)
    |> List.flatten()
    |> Enum.uniq()
    |> Enum.filter(&(!is_nil(&1)))
    |> Enum.each(&(IO.inspect(&1)))
  end

  defp find_start_codon([], every_protein_string), do: Enum.reverse every_protein_string
  defp find_start_codon([hd|tl], every_protein_string) do
    case hd == "A" && Enum.at(tl, 0) == "U" && Enum.at(tl, 1) == "G" do
      true ->
        protein_string = List.to_string([hd|tl])
        |> string_to_codons()
        |> bad_codon_filter([])
        |> find_end_codon([])
        find_start_codon(tl, [protein_string|every_protein_string])
      false -> 
        find_start_codon(tl, every_protein_string)
    end
  end

  # removes any codons with less than 3 characters from given enum
  defp bad_codon_filter([], codons), do: Enum.reverse codons
  defp bad_codon_filter([hd|tl], codons) do
    case String.length(hd) == 3 do
      true -> bad_codon_filter(tl, [hd|codons])
      false -> bad_codon_filter(tl, codons)
    end
  end

  # finds end codon & returns protein string of given data
  defp find_end_codon([], _), do: nil
  defp find_end_codon([hd|tl], orf) do
    case hd in stop_codons() do
      true ->
        orf
        |> Enum.reverse()
        |> Prot.start()
      false ->
        find_end_codon(tl, [hd|orf])
    end
  end
end