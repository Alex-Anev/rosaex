defmodule Perm do
  
  @n 5

  def start(n \\ @n) do
    1..n
    |> Enum.to_list()
    |> permutations()
    |> print()
  end

  def permutations([]), do: [[]]
  def permutations(list), do: for elem <- list, rest <- permutations(list--[elem]), do: [elem|rest]

  def print(perms) do
    Enum.each(perms, &(IO.inspect/1))
    Enum.count(perms)
  end
end