defmodule Fib do
  
  def n, do: 28
  def k, do: 2

  def start, do: loop(1, 0, n())
  def loop(children, adults, months) when months == 1, do: children + adults
  def loop(children, adults, months), do: loop(adults * k(), children + adults, months - 1)

  ## \/ this shows a more verbose version of the second loop() function above
  # def loop(children, adults, months) do
  #   new_children = adults * k()
  #   new_adults = children + adults
  #   IO.inspect "MONTH: #{months}, CHILDREN: #{children}, ADULTS: #{adults}, TOTAL: #{children+adults}"
  #   loop(new_children, new_adults, months + 1)
  # end
  
end