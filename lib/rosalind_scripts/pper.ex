defmodule Pper do

  @n 91
  @k 8

  def start(n \\ @n, k \\ @k) do
    # P(n,k) = n! / (n-k)! # what's happening below

    [n_hd|n_tl] = Enum.to_list(1..n)
    n_fact = perms(n_tl, n_hd)

    nminusk = n - k
    [k_hd|k_tl] = Enum.to_list(1..nminusk)
    nk_fact = perms(k_tl, k_hd)

    divided = n_fact / nk_fact
    rem(round(divided), 1_000_000)
  end

  def perms([], perm_count), do: perm_count
  def perms([hd|tl], acc), do: perms(tl, hd * acc)
  
end

# P(n,k) = n! / (n-k)!
# |> rem(1_000_000)

# https://www.youtube.com/watch?v=UAvRq_dQFqc