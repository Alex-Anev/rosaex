defmodule Fibd do

  def n, do: 96 # months
  def m, do: 17 # months until death
  
  def start do
    ## R(n,m) = R(n-1) + R(n-2) - R(n-(m+1))
    fib(n()-1) + fib(n()-2) - fib(n()-(m()+1))

    # (n-1) + (n-2) - (n-(m+1))
    
    ## verbose version of first attempt
    # m1 = fib(n() - 1)
    # m2 = fib(n() - 2)
    # d = fib(n()-(m()+1))

    # IO.inspect("-1: #{m1}")
    # IO.inspect("-2: #{m2}")
    # IO.inspect("d: #{d}")

    # m1 + m2 - d
  end

  def fib(acc), do: fib(0, 1, acc)

  def fib(n1, n2, acc) when acc == 0, do: n1

  def fib(n1, n2, acc), do: fib(n2, n1 + n2, acc - 1)
end

# https://www.omnicalculator.com/math/fibonacci#formula-for-n-th-term