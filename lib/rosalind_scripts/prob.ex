defmodule Prob do

  # https://github.com/folz/math
  alias Math, as: M

  # TODO: make process that stores all this data
  @dna "GCCATCTCATGGTACGGGCGATTACCCTCAAGCACTTGAGCTAAGCTGAACCGCTGGCTTTTACCGTAGGACATGTCCTCC"
  @arr_a [0.091, 0.131, 0.185, 0.241, 0.271, 0.318, 0.388, 0.444, 0.462, 0.523, 0.567, 0.626, 0.671, 0.718, 0.750, 0.801, 0.856, 0.931]

  def start(dna \\ @dna, arr_a \\ @arr_a) do
    gcat = dna
      |> String.split("", trim: true)
      |> gc_at_counter({0,0})
    # gcat should be stored in a process and fetched when needed
    logger(arr_a, gcat, [])
  end

  def gc_at_counter([], gc_at), do: gc_at
  def gc_at_counter([hd|tl], {gc, at}) do
    case hd in ["G", "C"] do
      true -> gc_at_counter(tl, {gc + 1, at})
      false -> gc_at_counter(tl, {gc, at + 1})
    end
  end

  def logger([], _, arr_b), do: Enum.reverse arr_b
  def logger([hd|tl], {gc, at}, arr_b) do
    # log10(hd/2)^gc * log10((1-hd)/2)^at
    x = (hd/2) |> M.pow(gc)
    y = ((1-hd)/2) |> M.pow(at)
    prob = M.log10(x*y) |> Float.ceil(3)
    logger(tl, {gc, at}, [prob|arr_b])
  end
end