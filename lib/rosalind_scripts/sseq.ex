defmodule Sseq do

  @dna "CAGCACGCGCTAAGTCTTTGTCGGAAAATTGGCACTGTCGCAATTGCCTGCATCCAGCCAACTAGGGCTCACGCTACCTTGACAACGGTCAGCCAATCATCCGGGCTCCGGGATATCTAAAAAACGGGTTCGGCTCGCAACGCGGCCGGCGGTAGACTCGCGAGAAACAGATATTAAGTAGCGGCTGGCTTCGCTAGCTAATGTAGCAGGATCGAATCGACTCCATGATAAACACGGAATGCCTACATAGGTTTTAATGCACTTGATATCCAGCCCCCCTCTAATTCGTGGTAGTAGTAGGTGGAGGTAGTGCCAAGGATCGTAGCTTCAAACAGCTGCTGGAGTCCATCTGTATTGGGGGTCCCCGTATTATATGCCGAGGAGAAGGTCGATATAGTGTACGGAAGTAAACACATCCTCAAATCCGAGGAATGAGTAACCACGAAGCGCGTAAAAGCTTGAAACCAGGACGCGTGACATAAAGCCTACTTTATGGGTCGTATACATTTCCGGAGGGCTCGATACTGGTCACGTAACGTCTACGCCCCTAAGATTGCCGTTTCGTAGAGGTGTAGACCTGGATACGCTATCTTGAAGCAGATGGGGTAGAGCGTAAGTGGTCTCAAGATAAAGCACCTAAGGATTGATAAGTGAGACTACAGCAAACAGAATCGCCTTAAAGTAAAGATCTTCGCCGCCACAGGTGACATCCCAACCAGAGTCCCTGCTCGGCAACCCGCTGATCCTGACAAATTGCATCTTGTCGGACAAGTTTTTGTGCTCGCCGGGCTTAACTACATCATCGACGCTGCTTAGCGTGGGGACTCGACCTCCTTCGTGCCCTGATGAACCACGGGAACGCTCATGGTAACGATGCAATCTATTGTGGCTAGCTGCTGTTGAAACAAG"
  @substr "TCTAAAATAGTGGGACCGTAAGATGGGAGATGCTTAGTTCATAGAG"

  def start(dna \\ @dna, substr \\ @substr) do
    s = String.split(dna, "", trim: true)
    t = String.split(substr, "", trim: true)
    loop(s, t, 1, {})
  end

  def loop(_, [], _index, answer), do: answer

  def loop([s_hd|s_tl], [t_hd|t_tl], index, answer) do
    cond do
      s_hd == t_hd ->
        loop(s_tl, t_tl, index + 1, Tuple.append(answer, index))
      s_hd != t_hd ->
        loop(s_tl, [t_hd|t_tl], index + 1, answer)
      true -> IO.puts "err"
    end
  end
end