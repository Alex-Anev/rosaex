defmodule Iev do
  # genotype: number of couples w/ said genotype
  # each couple has the same genotype
  @genotypes [
    # kk 100%
    xxxx: 17646,
    # km 100%
    xxxy: 17809,
    # kn 100%
    xxyy: 16323,
    # mm 75%
    xyxy: 18922,
    # mn 50%
    xyyy: 16912,
    # nn 0%
    yyyy: 18008
  ]

  # amount of offspring each couple will have
  @offspring_num 2

  def start(genotypes \\ @genotypes, offspring_num \\ @offspring_num) do
    Agent.start(fn -> offspring_num end, name: :offspring_num)

    genotypes
    |> calc_offspring([])
    |> count_dom(0)
  end

  # calculates amount of offpspring each couple will have
  defp calc_offspring([], result), do: Enum.reverse(result)

  defp calc_offspring([hd | tl], result) do
    new_hd = {elem(hd, 0), elem(hd, 1) * Agent.get(:offspring_num, & &1)}
    calc_offspring(tl, [new_hd | result])
  end

  # counts up how many of the offspring will have dominant phenotype
  defp count_dom([], result), do: trunc(result)

  defp count_dom([hd | tl], result) do
    cond do
      elem(hd, 0) == :yyyy -> count_dom(tl, result + 0)
      elem(hd, 0) == :xyyy -> count_dom(tl, result + elem(hd, 1) * 0.50)
      elem(hd, 0) == :xyxy -> count_dom(tl, result + elem(hd, 1) * 0.75)
      true -> count_dom(tl, result + elem(hd, 1))
    end
  end
end
