defmodule Gc do

	def start(data) do
		Enum.map(data, fn x ->
			percentage = dna_iterator(elem(x, 1))
			Tuple.append(x, percentage)
		end)
		|> Enum.max(fn {_, _, val1}, {_, _, val2} -> val1 > val2 end)
	end

	def dna_iterator(enum) do
		dna_str = String.split(enum, "", trim: true)
		gc_count = gc_counter(dna_str, 0)
		(gc_count/length(dna_str))*100
	end

	def gc_counter([], gc_amount), do: gc_amount

	# counts every g & c
	def gc_counter([head|tail], counter) do
		case String.contains?(head, ["C", "G"]) do
			true -> gc_counter(tail, counter + 1)
			false -> gc_counter(tail, counter)
		end
	end
end