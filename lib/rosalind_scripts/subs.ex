defmodule Subs do

  # @s "AATAGAGTGGATCTTTGGATCTTTGGATCTTGGATCTTGGATCTATGGATCTTACGAAATTGGATCTGCAGGCGATGGATCTTTGGATCTAGTTGGATCTATGGATCTTGGATCTTGGATCTTGGATCTTGGATCTTTGGATCTGGGCTTGGATCTGATGGATCTGTGGATCTAGTGGATCTGTGGATCTCCGCTGGATCTTGGATCTTGGATCTTGGATCTCTGGATCTTTGGATCTTGCCCCCCGAACTGGATCTAAACTGCTTTGGATCTACTCTGGATCTGTGGATCTGATTTGGATCTTGGATCTACTCGAAAGGGAGTGGATCTTGGATCTTTGGATCTGTGTGGATCTTGGATCTTTACAGTGGATCTTGGATCTGTTGGATCTTGGATCTATGGATCTGCTATGGATCTGCTGGATCTTGGATCTTTTGGATCTTGGATCTATTTGGATCTCTGGATCTTTGGATCTTCTGGATCTCGCTGGATCTTGGATCTGTGGATCTTCCACTTGGATCTTGGATCTTGGATCTAACTGGATCTGTCTGGATCTTCTCCTGGATCTCTGGATCTACCTTGGATCTATGGATCTTGGATCTGTGGATCTGCTGATGGATCTTGGATCTGCGTGGATCTCTGGATCTAATTGGATCTCTAATGGATCTTGGATCTTTATGTGGATCTCAGGCCAGATCAGTTGGATCTTGGATCTTAGTGGATCTCTGGATCTTGGATCTTTGGATCTTGGATCTTGGATCTCTGGATCTTTGGATCTGCAATGGATCTCAGTGGATCTTTTAATGAATTACTGGATCTTCTGGATCTGCGCTGGATCTTTGGATCTTAATACCCGTGGATCTGACGGATTGGATCTTTGGATCTCTGGATCTTGGATCTCTGGATCTACGTGGATCTACTGGATCTCCGTTGGATCTTAGATGGATCTCACATGGATCT"
  # @t "TGGATCTTG"

  @s "GATATATGCATATACTT"
  @t "ATAT"

  def start(main_str \\ @s, sub_str \\ @t) do
    spawn(fn -> sub_str_storage(sub_str) end)
    |> Process.register(:substr)

    String.split(main_str, "", trim: true)
    |> dna_loop(1, {})
  end

  def sub_str_storage(sub_str) do
    sub_str = receive do
      {:get_str, from} -> send(from, sub_str)
    end
    sub_str_storage(sub_str)
  end

  def get_sub_str() do
    send(:substr, {:get_str, self()})
    receive do x -> x end
  end

  # end state, reads results & kills substring storage
  def dna_loop([], _index, match_index) do
    # Process.exit(:substr, :kill)
    match_index
  end

  # index_counter tracks location of every letter
  # when match is found the matching index is added to match_index
  def dna_loop([head|tail], index_counter, match_index) do
    case head == String.first(get_sub_str()) do
      true ->
        [_|t_tail] = String.split(get_sub_str(), "", trim: true)
        case substring_loop(tail, t_tail, 0) do
          true -> 
            dna_loop(tail, index_counter + 1, Tuple.append(match_index, index_counter))
          false -> dna_loop(tail, index_counter + 1, match_index)
        end
      false -> 
        dna_loop(tail, index_counter + 1, match_index)
    end
  end

  # second end state in case main string runs out before substring
  def substring_loop([], _substr, substring_matches), do: String.length(get_sub_str()) - 1 == substring_matches

  # returns true or false to the case do block nested inside dna_loop
  # substring_matches counts how many letters match
  # makes sure the amount of matches are the same as the substring  length
  # String.lenght() - 1 because dna_loop sends the tail of the substring instead of entire substring
  # therefore String.length(substring) will always have 1 more than substring_matches
  def substring_loop(_mainstr, [], substring_matches), do: String.length(get_sub_str()) - 1 == substring_matches

  def substring_loop([s_head|s_tail], [t_head|t_tail], substring_matches) do
    cond do
      s_head == t_head ->
        substring_loop(s_tail, t_tail, substring_matches + 1)
      s_head != t_head ->
        substring_loop(s_tail, t_tail, substring_matches)
      true -> "error"
    end
  end

end