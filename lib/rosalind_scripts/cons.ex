defmodule Cons do
  def start(d) do
    listed_data = d |> Enum.map(fn {_id, str} -> split(str) end)

    dna_length =
      d
      |> hd()
      |> elem(1)
      |> String.length()

    indexes = Enum.to_list(1..dna_length)

    data =
      index_loop(indexes, listed_data, [])
      |> Enum.map(fn x -> Tuple.to_list(x) end)

    cons_data =
      data
      |> Enum.map(fn el ->
        Enum.find_index(el, fn x ->
          x === Enum.max(el)
        end)
      end)

    # gets consensus string
    consensus = cons_generator(cons_data, "")

    # reformats data
    matrix = create_matrix(data, [{}, {}, {}, {}])

    format_matrix(matrix, [])

    IO.inspect(consensus)
    IO.inspect(matrix, limit: :infinity)
    ## for rosalind \/
    # IO.inspect format_matrix(matrix, []), limit: :infinity
  end

  ## formats data for submission to rosalind.info
  ## adds nothing useful
  defp format_matrix([], result), do: Enum.reverse(result)

  defp format_matrix([hd | tl], result) do
    str =
      hd
      |> Tuple.to_list()
      |> Enum.join(" ")

    format_matrix(tl, [str | result])
  end

  ## this is so ugly, I am sorry
  defp create_matrix([], result), do: result

  defp create_matrix([hd | tl], [a, c, g, t]) do
    new_a = Tuple.append(a, Enum.at(hd, 0))
    new_c = Tuple.append(c, Enum.at(hd, 1))
    new_g = Tuple.append(g, Enum.at(hd, 2))
    new_t = Tuple.append(t, Enum.at(hd, 3))
    create_matrix(tl, [new_a, new_c, new_g, new_t])
  end

  ## makes consensus string from given indexes
  defp cons_generator([], consensus), do: consensus

  defp cons_generator([hd | tl], cons) do
    cond do
      hd == 0 -> cons_generator(tl, cons <> "A")
      hd == 1 -> cons_generator(tl, cons <> "C")
      hd == 2 -> cons_generator(tl, cons <> "G")
      hd == 3 -> cons_generator(tl, cons <> "T")
      true -> IO.puts("err")
    end
  end

  ## index_loop, data_loop & dna_head_del count up
  ## the values needed for the matrix
  defp index_loop([], _, matrix), do: matrix

  defp index_loop([hd | tl], data, matrix) do
    answer = data_loop(data, {0, 0, 0, 0})
    new_matrix = matrix ++ [answer]

    new_data = dna_head_del(data, [])

    index_loop(tl, new_data, new_matrix)
  end

  defp data_loop([], answer), do: answer

  defp data_loop([hd | tl], {a, c, g, t}) do
    cond do
      hd(hd) == "A" ->
        data_loop(tl, {a + 1, c, g, t})

      hd(hd) == "C" ->
        data_loop(tl, {a, c + 1, g, t})

      hd(hd) == "G" ->
        data_loop(tl, {a, c, g + 1, t})

      hd(hd) == "T" ->
        data_loop(tl, {a, c, g, t + 1})

      true ->
        IO.puts("err")
        data_loop(tl, {a, c, g, t})
    end
  end

  # this could be done inside index & data loop maybe
  defp dna_head_del([], result), do: result

  defp dna_head_del([hd | tl], result) do
    new_result = result ++ [tl(hd)]
    dna_head_del(tl, new_result)
  end

  defp split(str), do: String.split(str, "", trim: true)
end
