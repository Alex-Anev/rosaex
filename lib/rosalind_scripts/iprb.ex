defmodule Iprb do

  def k, do: 19 # XX # 2 homozygous dominant 
  def m, do: 26 # XY # 2 heterozygous
  def n, do: 17 # YY # 2 homozygous recessive
  def t, do: k() + m() + n() # total

  def start do
    kk = (k()/t())*((k()-1)/(t()-1)) # 100%
    km = (k()/t())*(m()/(t()-1)) # 100%
    kn = (k()/t())*(n()/(t()-1)) # 100%
    mk = (m()/t())*(k()/(t()-1)) # 100%
    mm = ((m()/t())*((m()-1)/(t()-1))*0.75) # 75%
    mn = ((m()/t())*(n()/(t()-1))*0.5) # 50%
    nk = (n()/t())*(k()/(t()-1)) # 100%
    nm = ((n()/t())*(m()/(t()-1))*0.5) # 50%
    nn = ((n()/t())*((n()-1)/(t()-1))*0.0) # 0%
    # -0.00001 is needed because Rosalind can't round numbers up
    (kk+km+kn+mk+mm+mn+nk+nm+nn)-0.00001
    |> Float.ceil(5)
  end
  # i know
  # i just wanna be done with this
end

# https://stackoverflow.com/questions/25119106/rosalind-mendels-first-law-iprb?answertab=votes#tab-top
# https://byjus.com/probability-formulas/
# https://elixirforum.com/t/generate-all-combinations-having-a-fixed-array-size/26196