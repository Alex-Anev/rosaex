# Dna

**TODO: Add description**

name is subject to change

Ignore controller.ex & data.ex

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `rosaex` to your list of dependencies in `mix.exs`: (its not available in hex)

```elixir
def deps do
  [
    {:rosaex, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/rosaex](https://hexdocs.pm/rosaex).

